package test

import (
	hl7 "bitbucket.org/cocoon_bitbucket/hl7nc/pkg"
	"bitbucket.org/cocoon_bitbucket/hl7nc/publisher"
	"bitbucket.org/cocoon_bitbucket/hl7nc/test/ecg"
	"fmt"
	"log"
	"testing"
	"time"
)

func TestEcgScreenPublisher(t *testing.T) {

	// create publisher
	pub := publisher.NewScreenPublisher("hl7nc")

	// create a fake ecg data source
	ecg1 := &ecg.FakeEcg{
		PatientId:   "14140f00-7bbc-0478-11122d2d02000000",
		PatientName: "WEERASINGHE^KESHARA",
		OBR:         "Mindray Monitor",
		BED:         "BED-001",
		NM51:        "100.0",
		NM52:        "189.0",
		ST2301:      "2",
	}

	start := time.Now()
	count := 0
	for finished := false; finished == false; {
		data, err := ecg1.Next()
		if err != nil {
			log.Printf("error reading ecg %s : %s", ecg1.Name(), err)
			finished = true
			break
		}
		count++
		//fmt.Println(string(data))

		msg, _ := hl7.NewHl7RawMessage(data)
		topic, err := msg.Topic()
		if err == nil {
			content, _ := msg.Content()
			pub.Publish(topic, content)
		}

		if count >= 5 {
			finished = true
		}

	}
	elapsed := time.Since(start)

	fmt.Printf("read [%d] lines in %s", count, elapsed)

}
