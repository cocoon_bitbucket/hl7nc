package connector

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
)

/*
	a fake nats ADT service

	* subscribe to hl7nc.hlt.ADT.>
	* reply with hl7 ACK or NACK

	a connector connects a nats service to a native service

	nats request server --> native service



	sample ADT message

	MSH|^~\&|REG|XYZ||XYZ|20050912110538||ADT^A04|4676115|P|2.3|
	EVN|A04|20050912110538|||VKP PID|||353966||SMITH^JOHN^^^^||19820707|F||C|108 MAIN STREET ^^ANYTOWN^TX^77777^^|HARV|(512)555-0170|||||00362103|123-45-6789||||||||||||
	NK1|1|SMITH^JOHN^|M|108 MAIN STREET^^ANYTOWN^TX^77777^^|(512)555-0170||NK|
	NK1|2|SMITH^STEPHEN^|F|108 MAIN STREET^^ANYTOWN^TX^77777^^|(512)555-0170||NK|
	NK1|1||||||EM|||UNEMPLOYED|||UNEMPLOYED^^UNEMP|||||||||||||||||||^^^^^^||3|
	PV1||O|SEROT|3|||1284^JOHNSON^MIKE^S.^^MD~|||SEROT||||1|||1284^JOHNSON^MIKE^S.^^MD|SERIES|787672|B|||||||||N||||||||||||A|||20050912110230||||||
	PV2|||HAND BRACE NEEDS REPAIRED|||||||||||20050912||||||||||A||20050725|||||O|||||| DG1|000||""^^I9|||A||||||||||| GT1|1||SMITH^JOHN^||108 MAIN STREET^^ANYTOWN^TX^77777^^|(512)555-0170||19820707|F||P|123-45-6789|||1|UNEMPLOYED|||||||||||||UNEMP|S|||||||||||||||||||UNEMPLOYED||UNEMPLOYED^^UNEMP|

	sample ACK
	MSH|^~\&|SFConnect|SecureFlow Pro||| 20030226000000||ACK|6162003124232500|P|2.3|||||||MSA|AA|20030226000000|Success||||

	The following is an example failure ACK message.

	MSH|^~\&|SFConnect|SecureFlow Pro ||| 20030226000000||ACK|6162003124232500|P|2.3|||||||MSA|AE|20030226000000|Failure||||


*/

const (
	ACK = `MSH|^~\&|SFConnect|SecureFlow Pro||| 20030226000000||ACK|6162003124232500|P|2.3|||||||MSA|AA|20030226000000|Success||||"
`
	NACK = `MSH|^~\&|SFConnect|SecureFlow Pro ||| 20030226000000||ACK|6162003124232500|P|2.3|||||||MSA|AE|20030226000000|Failure||||
`
	Subject = "hl7nc.hlc.ADT.>"
)

type NativeService struct {
	nc   *nats.Conn
	Nats string // the nats server url

}

func (s *NativeService) Adt(hl7 []byte) (ack []byte, err error) {

	// hl7 message of type ADT

	// return a fake ACK
	return []byte(ACK), nil
}

func (s *NativeService) handler(msg *nats.Msg) {

	// receive a hl7 nats message
	response, _ := s.Adt(msg.Data)
	s.nc.Publish(msg.Reply, []byte(response))

}

func (s *NativeService) Run(ctx context.Context) {

	// create nats connection
	conn, err := nats.Connect(s.Nats)
	if err != nil {
		log.Printf("cannot open nats connection: %s", err.Error())
		log.Fatalln(err)
	}
	defer conn.Close()
	s.nc = conn
	msg := fmt.Sprintf("Nats Service ADT connected with : %s\n", s.Nats)
	log.Println(msg)

	// subscribe to hl7nc.hl7.ADT.>
	sub, err := s.nc.Subscribe(Subject, s.handler)
	if err != nil {
		log.Fatalln(err)
	}

	// blocking wait for Done signal
	select {
	case <-ctx.Done():
		sub.Unsubscribe()
	}
	log.Print("Nats Service ADT deconnected")

}
