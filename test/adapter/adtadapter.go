package adapter

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"time"
)

/*
	an adapter send a hl7 request to nats bus and get response

	adapt a client to a nats bus

	usage :

		c , _ := NewAdtConnector( "nats:127.0.0.0:4222" )
		ack,err := adt.Send( hl7Message )




*/

const (
	Subject = "hl7nc.hl7.ADT._"
	Timeout = 5 * time.Second
)

type AdtAdapter struct {
	nc      *nats.Conn
	Nats    string // the nats server url
	Subject string
	Timeout time.Duration
}

func NewAdtConnector(server string) (connector *AdtAdapter, err error) {

	connector = &AdtAdapter{
		nc:      nil,
		Nats:    server,
		Subject: Subject,
		Timeout: Timeout,
	}
	err = connector.Connect()
	return
}

func (s *AdtAdapter) Send(hl7 []byte) (ack []byte, err error) {

	// send hl7 request to nats
	response, err := s.nc.Request(s.Subject, hl7, s.Timeout)
	if err != nil {
		log.Printf("error sendind adt request: %s\n", err.Error())
	} else {
		ack = response.Data
	}
	return
}

func (s *AdtAdapter) Connect() (err error) {

	// create nats connection
	conn, err := nats.Connect(s.Nats)
	if err != nil {
		log.Printf("cannot open nats connection: %s", err.Error())
		log.Fatalln(err)
	}
	defer conn.Close()
	s.nc = conn
	msg := fmt.Sprintf("Nats Adt Connector connected with : %s\n", s.Nats)
	log.Println(msg)

	return
}
