package ecg

import (
	"log"
	"testing"
)

func TestEcg(t *testing.T) {

	p := &FakeEcg{
		PatientId:   "14140f00-7bbc-0478-11122d2d02000000",
		PatientName: "WEERASINGHE^KESHARA",
		OBR:         "Mindray Monitor",
		BED:         "BED-001",
		NM51:        "100.0",
		NM52:        "189.0",
		ST2301:      "2",
	}

	// generate next message
	p.ComputeNext()

	hl7, err := p.Hl7()
	if err != nil {
		t.Error(err)
		return
	}
	_ = hl7

	hl7Frame, err := p.Hl7Frame()
	if err != nil {
		t.Error(err)
		return
	}
	_ = hl7Frame

	s, err := p.String()
	if err != nil {
		t.Error(err)
		return
	}
	println(s)

}

func TestEcgDataSource(t *testing.T) {

	p := &FakeEcg{
		PatientId:   "14140f00-7bbc-0478-11122d2d02000000",
		PatientName: "WEERASINGHE^KESHARA",
		OBR:         "Mindray Monitor",
		BED:         "BED-001",
		NM51:        "100.0",
		NM52:        "189.0",
		ST2301:      "2",
	}

	for i := 0; i <= 5; i += 1 {
		hl7, err := p.Next()
		if err != nil {
			log.Printf("%s\n", err)
			continue
		}
		_ = hl7
	}

}
