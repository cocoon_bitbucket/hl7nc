package ecg

import (
	"bytes"
	"strconv"
	"strings"
	"text/template"
)

/*

	a fake ecg.icu  ( collector )

	device --> nats

	collect device data and publish to hl7nc/hl7/<Type>/<subType>

	usage :
		ecg := &FakeEcg{ ... }
		hl7,_ := p.Next()

*/

const (
	VT = byte(0x0B)
	FS = byte(0x1C)
	CR = byte(0x0D)
)

var base = `MSH|^~\&|||||||ORU^R01|103|P|2.3.1|
PID|||{{.PatientId}}||{{.PatientName}}||19960714|M|
PV1||I|^^ICU&1&3232237756&4601&&1|||||||||||||||A|||
OBR||||{{.OBR}}|||0|
OBX||NM|52^||{{.NM52}}||||||F
OBX||NM|51^||{{.NM51}}||||||F
OBX||ST|2301^||{{.ST2301}}||||||F
OBX||CE|2302^Blood||0^N||||||F
OBX||CE|2303^Paced||2^||||||F
OBX||ST|2308^BedNoStr||{{.BED}}||||||F
`

type FakeEcg struct {
	PatientId   string // 14140f00-7bbc-0478-11122d2d02000000
	PatientName string // WEERASINGHE^KESHARA

	OBR string // Mindray Monitor
	BED string // BED-001

	NM51   string // 100.0
	NM52   string // 189.0
	ST2301 string // 2

	seq int
}

//
// implements datasource interface
//
func (p *FakeEcg) Name() string {

	return "FakeECG_" + p.BED
}

func (p *FakeEcg) Next() ([]byte, error) {
	p.ComputeNext()
	hl7, err := p.Hl7()
	return hl7, err
}

// compute next fake data
func (p *FakeEcg) ComputeNext() {

	// transform variable
	p.seq += 1
	p.NM51 = strconv.Itoa(p.seq)
	p.NM52 = strconv.Itoa(p.seq + 2)
	p.ST2301 = strconv.Itoa(p.seq % 2)

}

// return a hlm7 message
func (p *FakeEcg) Hl7() (msg []byte, err error) {

	buf := &bytes.Buffer{}
	t := strings.Replace(base, "\n", "\r", -1)

	tmpl, err := template.New("icu").Parse(t)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(buf, p)
	if err != nil {
		panic(err)
	}

	return buf.Bytes(), err
}

// return an hl7Frame VT + hl7 + FS + CR
func (p *FakeEcg) Hl7Frame() (msg []byte, err error) {

	hl7, err := p.Hl7()
	if err != nil {
		return msg, err
	}
	msg = append(msg, VT)
	msg = append(msg, hl7...)
	msg = append(msg, FS)
	msg = append(msg, CR)

	return
}

func (p *FakeEcg) String() (msg string, err error) {

	buf := &bytes.Buffer{}
	//t:= strings.Replace(base,"\n","\r",-1)

	tmpl, err := template.New("icu").Parse(base)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(buf, p)
	if err != nil {
		panic(err)
	}

	return buf.String(), err
}

// produce a hlm7 message
func Hl7Message(p *FakeEcg) (msg []byte, err error) {

	buf := &bytes.Buffer{}
	t := strings.Replace(base, "\n", "\r", -1)

	tmpl, err := template.New("icu").Parse(t)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(buf, p)
	if err != nil {
		panic(err)
	}
	return buf.Bytes(), err
}
