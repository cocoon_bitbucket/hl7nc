# hl7nc #

HL7 nats connector or HL7HUB


transform a hl7 message stream into a nats stream


the lib is a set of tools
to collect hl7 message from devices and publish them to nats hl7 messages
serve as a base to create a nats HUB for hl7 messages

* decouple data acquisition from usage
* permits subscription to available message streams


eg subscribes to hl7nc/hl7/ORU/R01  to get all the messages of type ORU-R01 published on the bus
whithout to be aware of the network topology and localisation of the source

The messages received are plain HL7 Messages
so no loss of information


# Nats slideshare

https://www.slideshare.net/nats_io/nats-connect-live-232305947
https://www.slideshare.net/nats_io/deploy-secure-and-scalable-services-across-kubernetes-clusters-with-nats

