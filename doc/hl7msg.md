
# MSH

## doc

    https://www.lyniate.com/knowledge-hub/hl7-msh-message-header




# ORU

## doc

* https://www.lyniate.com/knowledge-hub/hl7-oru-message/
* http://hl7reference.com/HL7%20Specifications%20ORM-ORU.PDF


## notes

In the ORU message, the OBR (Observation request) and OBX (Observation) segments are most significant due to their functions:

    The HL7 OBR segment is used in all ORU messages as a report header, and contains important information
        about the order being fulfilled (i.e. order number, request date/time, observation date/time,
        ordering provider, etc.). This segment is part of a group that can be used more than once
        for each observation result that is reported in the message.

    The HL7 OBX segment transmits the actual clinical observation results as a single observation or observation fragment.
        OBX segments can also be used more than once in the message, and may be followed by one or more NTE segments
        to provide additional notes and comments about the observation.
