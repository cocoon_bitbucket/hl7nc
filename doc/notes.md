

# hl7 tuto


https://hl7soup.com/HL7Tutorials.html


## interfaceware

https://blog.interfaceware.com/understanding-hl7-messages/


ack message: https://blog.interfaceware.com/the-acknowledgment-protocol/


# topics 

* Electronic health recorder EHR
* Health system ICU


For numeric vital signs observations, a normative mapping from ISO/IEEE 11073 to SNOMED and/or LOINC are developed (by organizations representing SNOMED and/or LOINC) and are made available on the NIST RTMMS and other nomenclature repositories.  Enterprise “Device Observation Consumers” would use the normative mapping table to translate the ISO/IEEE 11073 nomenclature to SNOMED and/or LOINC.

Device-centric data (waveforms, events, alerts, configuration and commands) would use the ISO/IEEE 11073 nomenclature.

# nats topic elements

* prefix   hl7
* udi  universal device id
* H7L message type   ADT/
* message suptype ?

eg hl7/AccMgr.1/ADT/A01


sample 

	MSH|^~\&|AccMgr|1|||20050110045504||ADT^A01|599102|P|2.3||| EVN|A01|20050110045502||||| 


MSH 3 : AccMgr   , Sending Application
MSH 4:  1   	 , Sending Facility

MSH 7 : 20050110045504   date/time

MSH 9.1 ADT		Message Type  
MSH 9.2 A01 



icu hl7 sample

	MSH|^~\&|||||||ORU^R01|103|P|2.3.1|<CR>
	PID|||14140f00-7bbc-0478-11122d2d02000000||WEERASINGHE^KESHARA||19960714|M|<CR>
	PV1||I|^^ICU&1&3232237756&4601&&1|||||||||||||||A|||<CR>
	OBR||||Mindray Monitor|||0|<CR>
	OBX||NM|52^||189.0||||||F<CR>
	OBX||NM|51^||100.0||||||F<CR>
	OBX||ST|2301^||2||||||F<CR>
	OBX||CE|2302^Blood||0^N||||||F<CR>
	OBX||CE|2303^Paced||2^||||||F<CR>
	OBX||ST|2308^BedNoStr||BED-001||||||F<CR>

topic: hl7/./ORU/R01


# hl7 sri lanka

	https://kasvith.me/posts/how-we-created-a-realtime-patient-monitoring-system-with-go-and-vue/


hl7 lib

* blog	https://www.fknsrs.biz/blog/golang-hl7-library.html

* repo https://github.com/deoxxa/hl7

some other libs

* https://github.com/kdar/health




# nats doc

    https://www.slideshare.net/Apcera/nats-vs-http
