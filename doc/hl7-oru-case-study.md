
# hl7 ORU message case study


# sample

    MSH|^~\&|||||||ORU^R01|103|P|2.3.1|<CR>
    PID|||14140f00-7bbc-0478-11122d2d02000000||WEERASINGHE^KESHARA||19960714|M|<CR>
    PV1||I|^^ICU&1&3232237756&4601&&1|||||||||||||||A|||<CR>
    OBR||||Mindray Monitor|||0|<CR>
    OBX||NM|52^||189.0||||||F<CR>
    OBX||NM|51^||100.0||||||F<CR>
    OBX||ST|2301^||2||||||F<CR>
    OBX||CE|2302^Blood||0^N||||||F<CR>
    OBX||CE|2303^Paced||2^||||||F<CR>
    OBX||ST|2308^BedNoStr||BED-001||||||F<CR>


# OBX Observation Result Segment

Pos Element                 Length  Use Example
1   Set ID (OBX)            10      R   1, 2, 3, ...
2   Value Type              2       R   AD = Address
                                        DT Date
                                        ED Encapsulated Data
                                        FT Formatted Text (Display)
                                        ST String Data.
                                        TM Time
                                        TN Telephone Number
                                        TS Time Stamp (Date & Time)
                                        TX Text Data (Display)

3   Observation Identifier  590     R EX00569
4   Observation Sub-ID      20      U
5   Observation Value       65536   O   Report section text.
6   Units                   60      U
7   References Range        10      U
8   Abnormal Flags          5       U
9   Probability             5       U


##  Lines

 OBX||NM|52^||189.0||||||F<CR>

 2 valueType:  NM  (numerical)
 3 identifier: 52^
 5 value     : 189.0

OBX||NM|51^||100.0||||||F<CR>

 2 valueType:  NM  (numerical)
 3 identifier: 51^
 5 value     : 100.0

OBX||ST|2301^||2||||||F<CR>

 2 valueType:  ST  (string)
 3 identifier: 2301^
 5 value     : 2

OBX||CE|2302^Blood||0^N||||||F<CR>

  2 valueType:  CE
  3 identifier: 2302^Blood
  5 value     : 0^N

OBX||CE|2303^Paced||2^||||||F<CR>

  2 valueType:  CE
  3 identifier: 2303^Paced
  5 value     : 2^


OBX||ST|2308^BedNoStr||BED-001||||||F<CR>

   2 valueType:  ST  (string)
   3 identifier: 2308^BedNoStr
   5 value     : BED-001


# OBR Observation Request (OBR) Segment

Pos Element                 Length  Use  Example
1   Set ID                  4       R   1
2   Placer Order Number     75      R   EX00569
3   Filler Order Number     75      R   EX00569
4   Universal Service ID    200     U
5   Priority                2       U


##  Lines

OBR||||Mindray Monitor|||0|<CR>

4  Universal Service ID= Mindray Monitor


# PID Patient Identifier Segment

Pos Element                         Length Use  Example

1   Set ID (Patient ID)             4       U
2   Patient ID – External ID        20      R   123456
3   Patient ID – Internal ID        20      U
4   Alternate Patient ID            20      U
5   Patient Name
    1.  Last Name
    2.  First Name
    3.  Middle Initial
    4.  Suffix
    5.  Prefix                      48      R   Smith^John^C^Rev^III
6   Mother’s Maiden Name            48      U
7   Date and Time of Birth          26      O   YYYYMMDDhhmmss 19730704063200
8   Sex                             1       O   00111

## lines

PID|||14140f00-7bbc-0478-11122d2d02000000||WEERASINGHE^KESHARA||19960714|M|<CR>

 3 PatientID – Internal ID= 14140f00-7bbc-0478-11122d2d02000000
 5 PatientName  = WEERASINGHE^KESHARA
 7 Date and Time of Birth = 19960714
 8 Sex = M


# PV1 Patient Visit 1 Segment

Pos Element                     Length Use  Example
1   Set ID PV1                  4       U
2   Patient Class               1       O   E = Emergency
                                            I  = Inpatient
                                            O = Outpatient
                                            P = Preadmit
                                            R = Recurring Patient
                                            B = Obstetrics
3   Assigned Patient Location   80      U
4   Admission Type              2       O   A = Accident
                                            E = Emergency
                                            L = Labor and Delivery
                                            R = Routine
5 Preadmit Number               20      O
6 Prior Patient Location        80      U
7 Attending Doctor
    1.  Last Name
    2.  First Name
    3.  Middle Initial          60      O Smith^John^C


## lines

 PV1||I|^^ICU&1&3232237756&4601&&1|||||||||||||||A|||<CR>

3 Assigned Patient Location = ^^ICU&1&3232237756&4601&&1
