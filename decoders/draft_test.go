package decoders

import (
	. "fknsrs.biz/p/hl7"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseOneSegment(t *testing.T) {
	a := assert.New(t)

	m, d, err := ParseMessage([]byte(`MSH|^~\&|IPM|1919|SUPERHOSPITAL|1919|20160101000000||ADT^A08|555544444|D|2.4|||AL|NE`))
	a.NoError(err)
	a.Equal(&Delimiters{'|', '^', '~', '\\', '&'}, d)
	a.Equal(Message{
		Segment{
			Field{FieldItem{Component{"MSH"}}},
			Field{FieldItem{Component{"|"}}},
			Field{FieldItem{Component{"^~\\&"}}},
			Field{FieldItem{Component{"IPM"}}},
			Field{FieldItem{Component{"1919"}}},
			Field{FieldItem{Component{"SUPERHOSPITAL"}}},
			Field{FieldItem{Component{"1919"}}},
			Field{FieldItem{Component{"20160101000000"}}},
			nil,
			Field{FieldItem{
				Component{"ADT"},
				Component{"A08"},
			}},
			Field{FieldItem{Component{"555544444"}}},
			Field{FieldItem{Component{"D"}}},
			Field{FieldItem{Component{"2.4"}}},
			nil,
			nil,
			Field{FieldItem{Component{"AL"}}},
			Field{FieldItem{Component{"NE"}}},
		},
	}, m)

	_ = m
	_ = d
	_ = err
	msh9_1, _ := ParseQuery("MSH-9-1")
	msh9_2, _ := ParseQuery("MSH-9-2")

	fmt.Printf("%s.%s\n", msh9_1.GetString(m), msh9_2.GetString(m))

}
