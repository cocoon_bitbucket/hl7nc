package publisher_test

import (
	"fmt"
	"log"
	"testing"
	"time"

	"bitbucket.org/cocoon_bitbucket/hl7nc/publisher"
	nats "github.com/nats-io/nats.go"
)

var (
	//nats_url = "nats://192.168.99.100:4222"
	nats_url = "nats://demo.nats.io:4222"
)

func TestNatsPublisher(t *testing.T) {

	//nats_url = "nats://demo.nats.io:4222"
	nats_url = "nats://127.0.0.1:4222"

	p, err := publisher.NewNatsPublisher("publisher", nats_url)
	//err := p.Connect()
	if err != nil {
		//t.Errorf("%s\n",err.Error())
		t.Error(err)
		return
	}
	defer p.Close()

	// Simple Async Subscriber
	p.Subscribe("publisher.foo", func(m *nats.Msg) {
		fmt.Printf("Received a message: %s\n", string(m.Data))
	})

	// Simple Publisher
	p.Publish("foo", []byte("Hello foo"))

	// Simple Sync Subscriber
	sub, err := p.SubscribeSync("publisher.bar")
	if err != nil {
		//log.Printf("error: %s", err.Error())
		t.Error(err)
		return
	}

	// Simple Publisher
	p.Publish("bar", []byte("Hello bar"))

	m, err := sub.NextMsg(1 * time.Second)
	if err != nil {
		t.Errorf("error waiting bar: %s", err.Error())
		return
	} else {
		fmt.Printf("received:%v\n", m.Data)
	}

	//// Channel Subscriber
	//ch := make(chan *nats.Msg, 64)
	//sub, err = nc.ChanSubscribe("foo", ch)
	//if err != nil {
	//	log.Printf("error: %s",err.Error())
	//}
	//
	//msg := <- ch
	//_=msg

	// Unsubscribe
	sub.Unsubscribe()

	// Requests

	// Replies
	p.Subscribe("publisher.help", func(m *nats.Msg) {
		p.Publish(m.Reply, []byte("I can help!"))
	})

	msg, err := p.Request("publisher.help", []byte("help me"), 1000*time.Millisecond)
	if err != nil {
		log.Printf("error: %s", err.Error())
		return
	}
	response := "no-response"
	if len(msg.Data) >= 0 {
		response = fmt.Sprintf("received response to help :%v\n", string(msg.Data))
	}
	println(response)

	time.Sleep(5 * time.Second)

}
