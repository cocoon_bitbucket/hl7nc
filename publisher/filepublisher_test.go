package publisher_test

import (
	"log"
	"testing"

	"bitbucket.org/cocoon_bitbucket/hl7nc/publisher"
)

func TestFilePublisher(t *testing.T) {

	filename := "/tmp/testfile"

	p, err := publisher.NewFilePublisher("file", filename)
	//err := p.Connect()
	if err != nil {
		log.Fatal(err.Error())
	}
	defer p.Close()

	// Simple Publisher
	p.Publish("foo", []byte("Hello foo"))

	// Simple Publisher
	p.Publish("bar", []byte("Hello bar"))

}
