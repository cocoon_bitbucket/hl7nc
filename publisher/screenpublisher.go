package publisher

import (
	"fmt"
	"strings"
)

// ScreenPublisher : a publisher to publish to screen ( for debug )
type ScreenPublisher struct {
	Topic   string // root topic to indentify publisher
	Counter uint64 // nb of messages published
}

// NewScreenPublisher : create a screen publisher with a root topic
func NewScreenPublisher(topic string) *ScreenPublisher {
	if topic == "" {
		topic = "root."
	}
	p := ScreenPublisher{Topic: topic}
	return &p
}

// Publish msg to screen
func (p *ScreenPublisher) Publish(topic string, message []byte) (err error) {

	// compute full topic
	//topic = p.Topic + topic
	subject := fmt.Sprintf("%s.%s", p.Topic, topic)
	display := strings.Replace(string(message), "\r", "\n", -1)
	// publish to screen
	println("screenpublisher: publish to [" + subject + "] the message:\n" + display + "\n")
	p.Counter += 1
	return err
}
