package publisher

import (
	"fmt"
	"log"
	"os"
)

// NatsPublisher : a publisher to publish to screen ( for debug )
type FilePublisher struct {
	*os.File
	Filename string
	Topic    string // root topic to indentify publisher
	Counter  uint64 // number of messages published
	// Mode string   // create or append
}

// NewFilePublisher : create a screen publisher with a root topic
func NewFilePublisher(topic string, filename string) (p *FilePublisher, err error) {
	if topic == "" {
		topic = "root."
	}
	mode := os.O_CREATE | os.O_WRONLY
	perm := os.FileMode(0666)
	h, err := os.OpenFile(filename, mode, perm)
	if err != nil {
		log.Printf("FilePublisher: cannot open file:%s\n", filename)
		return p, err
	}
	p = &FilePublisher{File: h, Topic: topic, Filename: filename}

	msg := fmt.Sprintf("FilePublisher: publish to file : %s\n", p.Filename)
	log.Println(msg)

	return p, err
}

// Close : close nats connection
func (p *FilePublisher) Close() {
	_ = p.File.Close()
	log.Printf("FilePublisher: closed\n")

}

// Publish msg to file
func (p *FilePublisher) Publish(topic string, message []byte) (err error) {

	// compute full topic
	//topic = p.Topic + topic
	subject := fmt.Sprintf("%s.%s", p.Topic, topic)
	body := string(message) + "\n\n"

	p.WriteString(subject)
	p.WriteString(": \n")
	p.WriteString(body)
	// publish to nats

	log.Printf("FilePublisher: publish to [" + subject + "] the message:\n" + string(message) + "\n")
	return err

}
