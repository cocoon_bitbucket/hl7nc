package pkg

import (
	"testing"
)

func TestParser(t *testing.T) {

	sample := `MSH|^~\&|EP\S\IC|EPICADT|SMS|SMSADT|199912271408|CHARRIS|ADT^A04|1817457|D|2.5|`

	msh, err := MshSplit([]byte(sample))
	if err != nil {
		t.Error(err)
	}
	//println(msh)

	app, _ := msh.SendingApp()
	mt, _ := msh.MessageType()

	if app != `EP\S\IC_EPICADT` {
		t.Error(err)
	}
	if mt != `ADT.A04` {
		t.Error(err)
	}

	println(app)
	println(mt)

	println("Done")
}
