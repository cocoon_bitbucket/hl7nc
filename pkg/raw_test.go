package pkg

import (
	"io/ioutil"
	"testing"
)

func TestRawParser(t *testing.T) {

	sample := `MSH|^~\&|EP\S\IC|EPICADT|SMS|SMSADT|199912271408|CHARRIS|ADT^A04|1817457|D|2.5|`

	msg, err := NewHl7RawMessage([]byte(sample))
	if err != nil {
		t.Error(err)
	}
	topic, err := msg.Topic()
	if err != nil {
		t.Error(err)
	}
	if topic != `hl7.ADT.A04` {
		//if topic != `hl7.EP\S\IC_EPICADT.ADT.A04` {
		t.Error(err)
	}

	println(topic)
	println("Done")
}

func TestRawParser2(t *testing.T) {

	fname := "../samples/sample.hl7"
	sample, err := ioutil.ReadFile(fname)
	if err != nil {
		t.Error(err)
		return
	}

	//sample := `MSH|^~\&|EP\S\IC|EPICADT|SMS|SMSADT|199912271408|CHARRIS|ADT^A04|1817457|D|2.5|`

	msg, err := NewHl7RawMessage([]byte(sample))
	if err != nil {
		t.Error(err)
		return
	}
	topic, err := msg.Topic()
	if err != nil {
		t.Error(err)
	}
	if topic != `hl7.ADT.A04` {
		//if topic != `hl7.EP\S\IC_EPICADT.ADT.A04` {
		t.Error(err)
	}

	println(topic)
	println("Done")
}
