package pkg

// a raw Hl7 message
//    topic is  hl7.<sender>.<type>.<SubType>    eg hl7.host_app.ADT.A04`

type Hl7RawMessage struct {
	data []byte
}

func NewHl7RawMessage(data []byte) (m *Hl7RawMessage, err error) {
	return &Hl7RawMessage{data}, nil
}

//
// implements Hl7Message
//
func (m *Hl7RawMessage) Content() (data []byte, err error) {
	return m.data, nil
}

// return raw topic  hl7.<type>.<SubType>
func (m *Hl7RawMessage) Topic() (topic string, err error) {

	msh, err := MshSplit([]byte(m.data))
	if err != nil {
		return
	}
	//app, _ := msh.SendingApp()
	mt, _ := msh.MessageType()
	topic = Hl7Prefix + "." + mt

	return topic, nil
}
