package pkg

const Hl7Prefix = "hl7"

type Hl7Message interface {
	Topic() (string, error)   // return the topic to publish the hl7 message
	Content() ([]byte, error) // a native hl7 message

}
