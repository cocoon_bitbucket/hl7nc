package pkg

import (
	"bytes"
	"fmt"
	"github.com/pkg/errors"
	"strings"
)

//    generic hl7 parsers

const MshHeader = `MSH|^~\&|`

// parse Msh segment

// MSH|^~\&|EP\S\IC|EPICADT|SMS|SMSADT|199912271408|CHARRIS|ADT^A04|1817457|D|2.5|

//
type MshSegments [][]byte

func (m MshSegments) SendingApp() (string, error) {
	if len(m) >= 4 {
		app := fmt.Sprintf("%s_%s", m[2], m[3])
		// remove dot
		app = strings.Replace(app, ".", "_", -1)
		return app, nil
	}
	return "", errors.New("Bad Hl7 Msh")
}
func (m MshSegments) MessageType() (string, error) {
	if len(m) >= 9 {

		mt := string(m[8])
		if mt != "" {
			mt := strings.Replace(mt, "^", ".", -1)
			return mt, nil
		}
		return mt, nil
	}
	return "", errors.New("Bad Hl7 Msh")
}

func MshSplit(data []byte) (parts MshSegments, err error) {

	if !bytes.Equal(data[:len(MshHeader)], []byte(MshHeader)) {
		err = errors.New("Invalid HL7 Message")
		return
	}
	parts = bytes.Split(data, []byte{'|'})
	return parts, nil
}
