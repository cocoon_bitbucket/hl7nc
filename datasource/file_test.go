package datasource_test

import (
	"bitbucket.org/cocoon_bitbucket/hl7nc/datasource"
	"fmt"
	"log"
	"testing"
	"time"
)

func TestFileSource(t *testing.T) {

	filename := "../samples/samplefile.hl7"

	s, err := datasource.NewFileSource(filename, "")
	if err != nil {
		log.Printf("cannot open %s\n", filename)
		t.Fail()
		return
	}
	defer s.Close()

	start := time.Now()
	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}
		count++
		fmt.Println(string(data))

	}
	elapsed := time.Since(start)

	fmt.Printf("read [%d] lines in %s", count, elapsed)

}
