package datasource

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"syscall"
	"time"
)

/*
	a data source from linux named pipe


*/

type PipeSource struct {
	*os.File
	Filename string // name of the pipe
	Reader   *bufio.Reader
	Delay    time.Duration // insert a delay after a Next() : to slow down the stream ,typically 10ms
}

// NewPipeSource create a handler on an existing linux namedPipe
func NewPipeSource(namedPipe string) (f *PipeSource, err error) {

	h, err := os.OpenFile(namedPipe, os.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		return f, err
	}
	reader := bufio.NewReader(h)

	f = &PipeSource{File: h, Filename: namedPipe, Reader: reader, Delay: 0}

	//f = &PipeSource{File: h, Filename: namedPipe}

	return f, err
}

//  implements Source interface

func (s *PipeSource) Name() string {
	return fmt.Sprintf("PipeSource(%s)", s.Filename)
}

// eager next
func (s *PipeSource) Next() (data []byte, err error) {

	for {
		data, err = s.Reader.ReadBytes('\n')
		if err == nil {
			// read ok : slow down stream
			if s.Delay != 0 {
				time.Sleep(s.Delay)
			}
			return data, nil
		}

		// reading error : reopen file
		_ = s.File.Close()
		s.Reopen(1 * time.Second)

	}
}

// eager reopen , wait forever until reopen
func (s *PipeSource) Reopen(wait time.Duration) {

	for {

		h, err := os.OpenFile(s.Filename, os.O_RDONLY, os.ModeNamedPipe)
		if err != nil {
			// cannot open file : wait a while and retry
			time.Sleep(wait)
			continue
		}
		s.File = h
		s.Reader = bufio.NewReader(h)
		return
	}

}

// helper tools

func CreateNamedPipe(name string) error {

	//_ = os.Remove(name)
	err := syscall.Mkfifo(name, 0666)
	if err != nil {
		if err.Error() == "file exists" {
			log.Printf("file exists\n")
			return nil
		}
		log.Printf("Make named pipe file error:%s\n", err)
	}
	return err
}

func GetPipeWriter(name string) (w *os.File, err error) {

	w, err = os.OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		log.Printf("error opening file: %v", err)
		return w, err
	}

	/* sample usage
	w.WriteString(fmt.Sprintf("test write times:%s\n", "hello world"))
	w.Close()
	*/
	return w, err
}

func GetPipeReader(namedPipe string) (r *os.File, err error) {

	r, err = os.OpenFile(namedPipe, os.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		log.Printf("error opening file: %v", err)
		return r, err
	}

	/* sample usage

	reader := bufio.NewReader(r)
	for {
		line, err := reader.ReadBytes('\n')
		if err == nil {
			fmt.Print("load string:" + string(line))
		}
	}

	*/

	return r, err
}

//
//	sample code
//

func sample_code(pipeFile string) {

	os.Remove(pipeFile)
	err := syscall.Mkfifo(pipeFile, 0666)
	if err != nil {
		log.Fatal("Make named pipe file error:", err)
	}
	go scheduleWrite(pipeFile)
	fmt.Println("open a named pipe file for read.")
	file, err := os.OpenFile(pipeFile, os.O_CREATE, os.ModeNamedPipe)
	if err != nil {
		log.Fatal("Open named pipe file error:", err)
	}
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadBytes('\n')
		if err == nil {
			fmt.Print("load string:" + string(line))
		}
	}
}

func scheduleWrite(pipeFile string) {
	fmt.Println("start schedule writing.")
	f, err := os.OpenFile(pipeFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	i := 0
	for {
		fmt.Println("write string to named pipe file.")
		f.WriteString(fmt.Sprintf("test write times:%d\n", i))
		i++
		time.Sleep(time.Second)
	}
}

/* Test result */
/*================================
go run pipe.go
open a named pipe file for read.
start schedule writing.
write string to named pipe file.
load string:test write times:0
write string to named pipe file.
load string:test write times:1
write string to named pipe file.
load string:test write times:2
=================================*/
