package datasource

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

/*

	an implementation of data source based on a hl7 file

	usage:

		source,err :=  NewFileSource( "filename", "10ms")
		for {
			next ,err := source.Next()
			...
		}


	frame

    VT (Vertical Tab)						FS(File Separator)		CR (Carriage Return)
	<0x0B>  				<HL7 Message> 		<0x1C>					<0x0D>


	hl7 message

	MSH| <........>   <0x0D>






*/

type FileSource struct {
	*os.File
	Filename string
	Reader   *bufio.Reader
	Delay    time.Duration // insert a delay after a Next() : to slow down the stream ,typically 1ms
}

var DefaultDelay = "10ms"

func NewFileSource(filename string, delay string) (f *FileSource, err error) {

	var t time.Duration = 0
	if delay != "" {
		t, err = time.ParseDuration(delay)
		if err != nil {
			// parsing error : set default delay
			t, _ = time.ParseDuration(delay)
		}
	}
	h, err := os.Open(filename)
	if err != nil {
		return f, err
	}
	reader := bufio.NewReader(h)

	f = &FileSource{File: h, Filename: filename, Reader: reader, Delay: t}

	return f, err
}

// implements datasource interface

func (s *FileSource) Next() (data []byte, err error) {

	// read until FS
	data, err = s.Reader.ReadBytes(0x1c)
	if err != nil {
		return data, err
	}

	// check next one is CR
	od, err := s.Reader.ReadByte()
	_ = od

	// slow down stream
	if s.Delay != 0 {
		time.Sleep(s.Delay)
	}
	// remove leading VT and trailing FS
	data = data[1 : len(data)-1]
	return data, err
}

func (s *FileSource) Name() string {
	return fmt.Sprintf("FileSource(%s)", s.Filename)
}

////
//// a combination of file source
////
//
//type FileSources struct {
//	name           string
//	SourceList     []string
//	SourceHandlers []*FileSource
//	Delay          time.Duration // insert a delay after a Next() : to slow down the stream ,typically 1ms
//	counter        int
//}
//
//func NewFileSources(delay string, filename ...string) (fs *FileSources, err error) {
//
//	var t time.Duration = 0
//	if delay != "" {
//		t, err = time.ParseDuration(delay)
//		if err != nil {
//			// parsing error : set default delay
//			t, _ = time.ParseDuration(delay)
//		}
//	}
//	fs = &FileSources{name: "composite", Delay: t}
//
//	for _, f := range filename {
//		fs.SourceList = append(fs.SourceList, f)
//		h, err := NewFileSource(f, delay)
//		if err != nil {
//			return fs, err
//		}
//		fs.SourceHandlers = append(fs.SourceHandlers, h)
//	}
//	return fs, err
//}
//
//func (s *FileSources) Name() string {
//	return fmt.Sprintf(s.name)
//}
//
//func (s *FileSources) Next() (data []byte, err error) {
//
//	for {
//		// read current file source
//		source := s.SourceHandlers[s.counter]
//		data, err = source.Next()
//		if err != nil {
//			if err.Error() != "EOF" {
//				// error
//				source.Close()
//				return []byte{}, err
//			} else {
//				// this is an EOF
//				source.Close()
//				s.counter += 1
//				if s.counter >= len(s.SourceHandlers) {
//					// the real EOF of all files
//					return []byte{}, err
//				}
//				// read the new file
//				continue
//			}
//		}
//		// ok
//		break
//	}
//
//	return data, err
//}
