package main

import (
	"io/ioutil"
	"log"
)

/* generate a sample file with several hl7 messages

	frame

    VT (Vertical Tab)						FS(File Separator)		CR (Carriage Return)
	<0x0B>  				<HL7 Message> 		<0x1C>					<0x0D>


	hl7 message

	MSH| <........>   <0x0D>




*/

const (
	VT = byte(0x0B)
	FS = byte(0x1C)
	CR = byte(0x0D)
)

func main() {

	var data []byte

	target := "samples/samplefile.hl7"

	fname := "samples/sample.hl7"

	sample, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatal("%s\n", err)
		return
	}

	for i := 0; i < 10; i++ {

		msg := []byte{}
		msg = append(msg, VT)
		msg = append(msg, sample...)
		msg = append(msg, FS)
		msg = append(msg, CR)

		data = append(data, msg...)
	}

	err = ioutil.WriteFile(target, data, 0666)
	if err != nil {
		log.Fatal(err)
	}

}
