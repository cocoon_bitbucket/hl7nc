package main

import (
	hl7 "bitbucket.org/cocoon_bitbucket/hl7nc/pkg"
	"bitbucket.org/cocoon_bitbucket/hl7nc/publisher"
	"bitbucket.org/cocoon_bitbucket/hl7nc/test/ecg"
	"log"
	"time"
)

func icu1(pub publisher.Publisher) {

	ecg := &ecg.FakeEcg{
		PatientId:   "14140f00-7bbc-0478-11122d2d02000000",
		PatientName: "WEERASINGHE^KESHARA",
		OBR:         "Mindray Monitor",
		BED:         "BED-001",
		NM51:        "100.0",
		NM52:        "189.0",
		ST2301:      "2",
	}

	for finished := false; finished == false; {
		data, err := ecg.Next()
		if err != nil {
			log.Printf("error reading ecg %s : %s", ecg.Name(), err)
			finished = true
			break
		}

		msg, _ := hl7.NewHl7RawMessage(data)
		topic, err := msg.Topic()
		if err == nil {
			content, _ := msg.Content()
			pub.Publish(topic, content)
		}
		time.Sleep(1000 * time.Millisecond)
	}

}

func icu2(pub publisher.Publisher) {

	ecg := &ecg.FakeEcg{
		PatientId:   "14140f00-7bbc-0478-11122d2d02000001",
		PatientName: "DUPONT^ALBERT",
		OBR:         "Mindray Monitor",
		BED:         "BED-002",
		NM51:        "100.0",
		NM52:        "189.0",
		ST2301:      "2",
	}

	for finished := false; finished == false; {
		data, err := ecg.Next()
		if err != nil {
			log.Printf("error reading ecg %s : %s", ecg.Name(), err)
			finished = true
			break
		}

		msg, _ := hl7.NewHl7RawMessage(data)
		topic, err := msg.Topic()
		if err == nil {
			content, _ := msg.Content()
			pub.Publish(topic, content)
		}
		time.Sleep(1100 * time.Millisecond)

	}
}

func main() {

	// create screen publisher
	pub := publisher.NewScreenPublisher("hl7nc")

	// launch ecg1
	go icu1(pub)

	// launch ecg2
	go icu2(pub)

	// time
	time.Sleep(10 * time.Second)

}
