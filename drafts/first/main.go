package main

import (
	"bitbucket.org/cocoon_bitbucket/hl7nc/datasource"
	hl7 "bitbucket.org/cocoon_bitbucket/hl7nc/pkg"
	"bitbucket.org/cocoon_bitbucket/hl7nc/publisher"
	"fmt"
	"log"
	"time"
)

func run() {

	// open a hl7 source file
	// for each message publish it

	filename := "./samples/samplefile.hl7"
	s, err := datasource.NewFileSource(filename, "")
	if err != nil {
		log.Printf("cannot open %s\n", filename)
		log.Print(err.Error())
		return
	}
	defer s.Close()

	// create publisher
	pub := publisher.NewScreenPublisher("hl7nc")
	//if err != nil {
	//	log.Printf("cannot open %s\n", filename)
	//	log.Print(err.Error())
	//	return
	//}

	start := time.Now()
	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}
		count++
		//fmt.Println(string(data))

		msg, _ := hl7.NewHl7RawMessage(data)
		topic, err := msg.Topic()
		if err == nil {
			content, _ := msg.Content()
			pub.Publish(topic, content)
		}

	}
	elapsed := time.Since(start)

	fmt.Printf("read [%d] lines in %s", count, elapsed)

}

func main() {

	sample := `MSH|^~\&|EP\S\IC|EPICADT|SMS|SMSADT|199912271408|CHARRIS|ADT^A04|1817457|D|2.5|`

	idx, err := hl7.MshSplit([]byte(sample))
	if err != nil {
		fmt.Errorf("%s\n", err)
	}
	println(idx)

	run()

}
