module bitbucket.org/cocoon_bitbucket/hl7nc

go 1.13

require (
	fknsrs.biz/p/hl7 v0.0.0-20180202102358-3a6d5d9de582
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/stackerr v0.0.0-20150612192056-c2fcf88613f4 // indirect
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/nats-io/nats-server/v2 v2.1.6 // indirect
	github.com/nats-io/nats.go v1.9.2
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.5.1
)
